package prueba.veritran.net.pruebauniversidad;

import java.util.HashMap;
import java.util.Map;

public class MontoEscrito {
    private static Map<Integer,String> parseoNumeros = new HashMap<Integer,String>();
    public static void main(String[] args) {

        System.out.println( MontoEscrito.getMontoEscrito(985756) );
    }

    public static String getMontoEscrito(Integer valor){

        String resultado = "";
        crearParseNumeros();

        if(parseoNumeros.containsKey(valor)){
            resultado =  parseoNumeros.get(valor);
        }else if((valor / 1000000)> 0){
            if((valor / 1000000)== 1){
                resultado = getMontoEscrito((valor/1000000)*1000000)+" " + getMontoEscrito((valor % 1000000));
            }else{
                resultado = getMontoEscrito(valor/1000000) + " millones " + getMontoEscrito((valor % 1000000));
            }
        }else if((valor / 1000) > 0 ){
            if((valor/1000) == 1) {
                resultado = getMontoEscrito((valor / 1000)*1000) +" "+ getMontoEscrito(valor%1000);
            }else{
                resultado = getMontoEscrito((valor / 1000)) + " mil "+  getMontoEscrito(valor%1000);
            }
        }else if((valor/100) > 0){

            if( valor / 100 == 1 ){
                resultado = "ciento " + getMontoEscrito(valor % 100);
            }else if ( (valor / 100 == 5) || (valor / 100 == 7) || (valor / 100 == 9)){
                resultado = getMontoEscrito((valor/100)*100) +" "+ getMontoEscrito(valor%100) ;
            }else{
                resultado = getMontoEscrito(valor/100) +"cientos "+  getMontoEscrito( valor%100);
            }

        }
        else if((valor/10) > 0){

            if((valor / 10) == 1 ){

                resultado = "dieci" + getMontoEscrito(valor % 10);

            }
            else if((valor/10 ) == 2){


                resultado = "veinti" + getMontoEscrito(valor % 10);

            }
            else{
                resultado = getMontoEscrito((valor/10 )* 10 ) + " y "+ getMontoEscrito(valor%10);
            }

        }else if(valor == 0){
            resultado = "";
        }




        return resultado;
    }

    private static void crearParseNumeros() {
        parseoNumeros.put(0,"cero");
        parseoNumeros.put(1,"uno");
        parseoNumeros.put(2,"dos");
        parseoNumeros.put(3,"tres");
        parseoNumeros.put(4,"cuatro");
        parseoNumeros.put(5,"cinco");
        parseoNumeros.put(6,"seis");
        parseoNumeros.put(7,"siete");
        parseoNumeros.put(8,"ocho");
        parseoNumeros.put(9,"nueve");
        parseoNumeros.put(10, "diez");
        parseoNumeros.put(11, "once");
        parseoNumeros.put(12, "doce");
        parseoNumeros.put(13, "trece");
        parseoNumeros.put(14, "catorce");
        parseoNumeros.put(15, "quince");
        parseoNumeros.put(20, "veinte");
        parseoNumeros.put(30, "treinta");
        parseoNumeros.put(40, "cuarenta");
        parseoNumeros.put(50, "cincuenta");
        parseoNumeros.put(60, "sesenta");
        parseoNumeros.put(70, "setenta");
        parseoNumeros.put(80, "ochenta");
        parseoNumeros.put(90, "noventa");
        parseoNumeros.put(100, "cien");
        parseoNumeros.put(500, "quinientos");
        parseoNumeros.put(700, "setecientos");
        parseoNumeros.put(900, "novecientos");
        parseoNumeros.put(1000, "mil");
        parseoNumeros.put(1000000, "un millon");
    }
}
